﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    private Transform tf; //variable to hold this gameObject's transform component
    private GameObject player; //variable to hold the player gameObject
    private Transform Ptf; //variable to hold the player's transform component

	// Use this for initialization
	void Start () {
        //initialize varables
        tf = GetComponent<Transform>();
        player = GameObject.Find("PlayerShip");
        Ptf = player.GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 NewPos = new Vector3(Ptf.position.x, Ptf.position.y, -10); //create a new vector at the camera's height with the player's position
        tf.position = NewPos; //set the camera's position to the new vector
	}
}
